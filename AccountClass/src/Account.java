public class Account {
    String id;
    String name;
    int balance = 0;

    public int getBalance() {
        return balance;
    }

    public Account() {
    }

    public Account(String id, String name, int balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }
    public Account(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int credit(int amount) {
        balance += amount;
        return balance;
    }

    public int debit(int amount) {
        if (amount <= balance){
            balance -= amount;
            return balance;
        }
        else {
            System.out.println("amount exceeded balance");
        }

        return balance;
    }

    public int transferTo(Account another, int amount) {
        if (amount <= balance){
            balance -= amount;
            another.balance += amount;
            return balance;
        }
        else {
            System.out.println("amount exceeded balance");
        }

        return balance;
    }

    public String toString() {
        return "Account[id= " + id + ", name= " + name + ", balance= " + balance +"]";
    }
}
