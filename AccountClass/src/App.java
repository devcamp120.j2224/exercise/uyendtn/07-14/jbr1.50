public class App {
    public static void main(String[] args) throws Exception {
        Account account1 = new Account("AC01", "OHMM");
        Account account2 = new Account("AC02", "Xtra", 1000);

        System.out.println(account1.toString());
        System.out.println(account2.toString());

        //tăng account 1 thêm 2000 và account 2 thêm 3000
        account1.credit(2000);
        System.out.println("sau khi tăng: " + account1.toString());
        account2.credit(3000);
        System.out.println("sau khi tăng: " + account2.toString());

        //giảm account1 1000 và account2 5000
        account1.debit(1000);
        System.out.println("sau khi giảm: " + account1.toString());

        account2.debit(5000);
        System.out.println("sau khi giảm: " + account2.toString());

        //chuyển 2000 từ account1 sang account2
        account1.transferTo(account2, 2000);
        System.out.println(account1.toString());
        System.out.println(account2.toString());

        //chuyển 2000 từ account2 sang account1
        account2.transferTo(account1, 2000);
        System.out.println(account1.toString());
        System.out.println(account2.toString());

}
}